resource "azurerm_linux_virtual_machine" "linux_vm" {
  count               = var.is_linux_vm ? 1 : 0
  name                = var.linux_vm_name
  resource_group_name = data.azurerm_resource_group.resource_group.name
  location            = data.azurerm_resource_group.resource_group.location
  size                = var.linux_vm_size
  admin_username      = var.linux_vm_admin_username
  network_interface_ids = [
    azurerm_network_interface.network_interface.id,
  ]

  admin_ssh_key {
    username   = var.linux_vm_admin_username
    public_key = var.linux_vm_admin_pub_key_path == "" ? null : file(var.linux_vm_admin_pub_key_path)
  }

  os_disk {
    caching              = var.linux_vm_os_disk_caching
    storage_account_type = var.linux_vm_os_disk_storage_type
  }

  dynamic "source_image_reference" {
    for_each = var.dg_custom_image ? [] : [1]
    content {
      publisher = var.source_image_publisher
      offer     = var.source_image_offer
      sku       = var.source_image_sku
      version   = var.source_image_version
    }
  }

  source_image_id = var.dg_custom_image ? data.azurerm_image.image_search[count.index].id : null

  tags = {
    "Name"        = var.linux_vm_name
    "Environment" = var.deployment_environment
    "Location"    = data.azurerm_resource_group.resource_group.location
    "Date"        = timestamp()
  }
}

resource "azurerm_managed_disk" "linux_persistent_data_disk" {
  count                = var.is_linux_vm ? 1 : 0
  name                 = var.persistent_data_disk_name
  location             = data.azurerm_resource_group.resource_group.location
  create_option        = var.persistent_data_disk_create_option
  disk_size_gb         = var.persistent_data_disk_size
  resource_group_name  = data.azurerm_resource_group.resource_group.name
  storage_account_type = var.persistent_data_disk_account_type
}

resource "azurerm_virtual_machine_data_disk_attachment" "linux_data_disk" {
  count              = var.is_linux_vm ? 1 : 0
  virtual_machine_id = azurerm_linux_virtual_machine.linux_vm[count.index].id
  managed_disk_id    = azurerm_managed_disk.linux_persistent_data_disk[count.index].id
  lun                = var.persistent_data_disk_lun
  caching            = var.persistent_data_disk_caching
}
