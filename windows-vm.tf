resource "azurerm_windows_virtual_machine" "windows_vm" {
  count               = var.is_linux_vm ? 0 : 1
  name                = var.windows_vm_name
  resource_group_name = data.azurerm_resource_group.resource_group.name
  location            = data.azurerm_resource_group.resource_group.location
  size                = var.windows_vm_size
  admin_username      = var.windows_vm_admin_username
  admin_password      = var.windows_vm_admin_password
  network_interface_ids = [
    azurerm_network_interface.network_interface.id,
  ]

  os_disk {
    caching              = var.windows_vm_os_disk_caching
    storage_account_type = var.windows_vm_os_disk_storage_type
  }

  dynamic "source_image_reference" {
    for_each = var.dg_custom_image ? [] : [1]
    content {
      publisher = var.source_image_publisher
      offer     = var.source_image_offer
      sku       = var.source_image_sku
      version   = var.source_image_version
    }
  }

  source_image_id = var.dg_custom_image ? data.azurerm_image.image_search[count.index].id : null

  tags = {
    "Name"        = var.windows_vm_name
    "Environment" = var.deployment_environment
    "Location"    = data.azurerm_resource_group.resource_group.location
    "Date"        = timestamp()
  }
}

resource "azurerm_managed_disk" "windows_persistent_data_disk" {
  count                = var.is_linux_vm ? 0 : 1
  name                 = var.persistent_data_disk_name
  location             = data.azurerm_resource_group.resource_group.location
  create_option        = var.persistent_data_disk_create_option
  disk_size_gb         = var.persistent_data_disk_size
  resource_group_name  = data.azurerm_resource_group.resource_group.name
  storage_account_type = var.persistent_data_disk_account_type
}

resource "azurerm_virtual_machine_data_disk_attachment" "windows_data_disk" {
  count              = var.is_linux_vm ? 0 : 1
  virtual_machine_id = azurerm_windows_virtual_machine.windows_vm[count.index].id
  managed_disk_id    = azurerm_managed_disk.windows_persistent_data_disk[count.index].id
  lun                = var.persistent_data_disk_lun
  caching            = var.persistent_data_disk_caching
}
