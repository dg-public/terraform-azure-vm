resource "azurerm_network_security_group" "network_security_group" {
  name                = var.network_security_group_name
  location            = data.azurerm_resource_group.resource_group.location
  resource_group_name = data.azurerm_resource_group.resource_group.name

  dynamic "security_rule" {
    for_each = var.security_group_rules
    content {
      name                                  = security_rule.value.name
      description                           = security_rule.value.description
      protocol                              = security_rule.value.protocol
      source_port_ranges                    = security_rule.value.source_port_ranges
      destination_port_ranges               = security_rule.value.destination_port_ranges
      source_address_prefix                 = security_rule.value.source_address_prefix
      destination_address_prefix            = security_rule.value.destination_address_prefix
      source_application_security_group_ids = security_rule.value.source_application_security_group_ids
      access                                = security_rule.value.access
      priority                              = security_rule.value.priority
      direction                             = security_rule.value.direction
    }
  }
}

resource "azurerm_application_security_group" "application_security_group" {
  count               = var.create_application_security_group ? 1 : 0
  name                = var.application_security_group_name
  location            = data.azurerm_resource_group.resource_group.location
  resource_group_name = data.azurerm_resource_group.resource_group.name
}
