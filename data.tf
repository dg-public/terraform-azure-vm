data "azurerm_resource_group" "resource_group" {
  name = var.resource_group_name
}

data "azurerm_subnet" "subnet" {
  name                 = var.subnet_name
  virtual_network_name = var.virtual_network_name
  resource_group_name  = var.resource_group_name
}

data "azurerm_image" "image_search" {
  count               = var.dg_custom_image ? 1 : 0
  name_regex          = var.image_search_pattern
  resource_group_name = var.resource_group_name
}

data "azurerm_application_security_group" "application_security_group" {
  count               = var.existing_application_security_group ? 1 : 0
  name                = var.application_security_group_name
  resource_group_name = var.resource_group_name
}
