resource "azurerm_network_interface" "network_interface" {
  name                    = var.network_interface_name
  location                = data.azurerm_resource_group.resource_group.location
  resource_group_name     = data.azurerm_resource_group.resource_group.name
  internal_dns_name_label = var.internal_dns_name_label

  dynamic "ip_configuration" {
    for_each = var.network_interface_ip_configuration
    content {
      name                                               = ip_configuration.value.name
      gateway_load_balancer_frontend_ip_configuration_id = ip_configuration.value.gateway_load_balancer_frontend_ip_configuration_id
      subnet_id                                          = data.azurerm_subnet.subnet.id
      private_ip_address_version                         = ip_configuration.value.private_ip_address_version
      private_ip_address_allocation                      = ip_configuration.value.private_ip_address_allocation
      public_ip_address_id                               = ip_configuration.value.public_ip_address_id
      private_ip_address                                 = ip_configuration.value.private_ip_address
    }
  }

  /*tags = {
    "Name"        = var.network_interface_name
    "Environment" = var.deployment_environment
    "Location"    = var.resource_group_location
    "Date"        = timestamp()
  }*/
}

resource "azurerm_network_interface_security_group_association" "nic_sg_association" {
  network_interface_id      = azurerm_network_interface.network_interface.id
  network_security_group_id = azurerm_network_security_group.network_security_group.id
}

resource "azurerm_network_interface_application_security_group_association" "existing_app_sg" {
  count                         = var.existing_application_security_group ? 1 : 0
  network_interface_id          = azurerm_network_interface.network_interface.id
  application_security_group_id = data.azurerm_application_security_group.application_security_group[count.index].id
}

resource "azurerm_network_interface_application_security_group_association" "new_app_sg" {
  count                         = var.create_application_security_group ? 1 : 0
  network_interface_id          = azurerm_network_interface.network_interface.id
  application_security_group_id = azurerm_application_security_group.application_security_group[count.index].id
}
