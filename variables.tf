variable "resource_group_name" {
  type = string
}

variable "deployment_environment" {
  type = string
}

variable "subnet_name" {
  type = string
}

variable "virtual_network_name" {
  type = string
}

variable "image_search_pattern" {
  type    = string
  default = ""
}

variable "dg_custom_image" {
  type    = bool
  default = false
}

variable "create_application_security_group" {
  type    = bool
  default = false
}

variable "existing_application_security_group" {
  type    = bool
  default = false
}

variable "network_security_group_name" {
  type = string
}

variable "application_security_group_name" {
  type    = string
  default = ""
}

variable "security_group_rules" {
  type = map(object({
    name                                       = string
    description                                = optional(string)
    protocol                                   = string
    source_port_ranges                         = list(string)
    destination_port_ranges                    = list(string)
    source_address_prefix                      = string
    destination_address_prefix                 = string
    source_application_security_group_ids      = optional(list(string))
    destination_application_security_group_ids = optional(list(string))
    access                                     = string
    priority                                   = number
    direction                                  = string
  }))
}

variable "network_interface_name" {
  type = string
}

variable "internal_dns_name_label" {
  type = string
}

variable "network_interface_ip_configuration" {
  type = map(object({
    name                                               = string
    gateway_load_balancer_frontend_ip_configuration_id = optional(string)
    private_ip_address_version                         = optional(string)
    private_ip_address_allocation                      = string
    public_ip_address_id                               = optional(string)
    private_ip_address                                 = optional(string)
  }))
}

variable "linux_vm_name" {
  type    = string
  default = null
}

variable "linux_vm_size" {
  type    = string
  default = "Standard_F2"
}

variable "linux_vm_admin_username" {
  type      = string
  sensitive = true
  default   = null
}

variable "linux_vm_admin_pub_key_path" {
  type    = string
  default = ""
}

variable "linux_vm_os_disk_caching" {
  type    = string
  default = "ReadWrite"
}

variable "linux_vm_os_disk_storage_type" {
  type    = string
  default = "Standard_LRS"
}

variable "is_linux_vm" {
  type = bool
}

variable "persistent_data_disk_size" {
  type = number
}

variable "persistent_data_disk_create_option" {
  type    = string
  default = "Empty"
}

variable "persistent_data_disk_name" {
  type = string
}

variable "persistent_data_disk_account_type" {
  type    = string
  default = "Standard_LRS"
}

variable "persistent_data_disk_lun" {
  type    = number
  default = 0
}

variable "persistent_data_disk_caching" {
  type    = string
  default = "None"
}

variable "windows_vm_name" {
  type    = string
  default = null
}

variable "windows_vm_size" {
  type    = string
  default = "Standard_F2"
}

variable "windows_vm_admin_username" {
  type      = string
  sensitive = true
  default   = null
}

variable "windows_vm_admin_password" {
  sensitive = true
  default   = null
}

variable "windows_vm_os_disk_caching" {
  type    = string
  default = "ReadWrite"
}

variable "windows_vm_os_disk_storage_type" {
  type    = string
  default = "Standard_LRS"
}

variable "source_image_publisher" {
  type    = string
  default = null
}

variable "source_image_offer" {
  type    = string
  default = null
}

variable "source_image_sku" {
  type    = string
  default = null
}

variable "source_image_version" {
  type    = string
  default = null
}
